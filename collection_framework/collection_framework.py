from typing import List
from collections import Counter
from functools import cache

@cache
def num_uniq_char(text: str) -> int:
    if not isinstance(text, str):
        raise TypeError(f'Input must be text!')
    else:
        number_uniq_char = len({key: val for key, val in Counter(text).items() if key != ' ' and key != '\n' and val == 1})
    return number_uniq_char


def num_uniq_char_list(list_of_string: List[str]) -> List[int]:
    if not isinstance(list_of_string, List):
        raise TypeError(f'Input must be List[str]!')
    number_uniq_char_list = map(num_uniq_char, list_of_string)
    return list(number_uniq_char_list)


if __name__ == '__main__':
    pass
