import pytest
from collection_framework.collection_framework import num_uniq_char, num_uniq_char_list


def test_num_uniq_char_Equal():
    cases = [
        ('qwerty', 6),
        ('12345 QWERTY', 11),
        ('qwerty QQWWEERRTTYY 11223345', 8),
        ('', 0)
    ]
    for text, number_uniq_char in cases:
        assert num_uniq_char(text) == number_uniq_char


def test_num_uniq_char_Type():
    cases = [
        (12345),
        ([12345, 'QWERTY']),
        (('qwerty', 'QQWWEE RRTTYY', 11223345))
    ]
    for case in cases:
        with pytest.raises(TypeError):
            num_uniq_char(case)


def test_num_uniq_char_list_Equal():
    cases = [
        (['qwerty'], [6]),
        (['12345 QWERTY'], [11]),
        (['qwerty', 'QQWWEERRTY', '112233456'], [6, 2, 3]),
        ([], [])
    ]
    for list_of_string, number_uniq_char_list in cases:
        assert num_uniq_char_list(list_of_string) == number_uniq_char_list


def test_num_uniq_char_list_Type():
    cases = [
        (12345),
        ([12345, 'QWERTY']),
        (('qwerty', 'QQWWEE RRTTYY', 11223345))
    ]
    for case in cases:
        with pytest.raises(TypeError):
            num_uniq_char_list(case)


if __name__ == '__main__':
    pytest.main()
